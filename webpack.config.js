"use strict";

module.exports = {
    entry: "./static/modules/MainApp.js",
    output: {
        path: __dirname + "/static/dist/",
        filename: "src/app.js"
    },
    module: {
        rules: [
            {
                test: /\.pug$/,
                use: "pug-loader"
            }
        ]
    },
    node: {
        fs: 'empty'
    }
};

# Пример применения шаблонизатора pug, препроцессора sass, сборщика webpack и ESLint

### package.json

> Первое правило - если уже есть конфиг фигачить копипастом из него и `npm install`

> Второе правило - `npm init` и устанавливаем зависимости

1. В разделе "scripts" указываем скрипты, которые будут запускаться npm
`npm start` в нашем примере выполнит команду `node index.js`.
Однако, при исполльзовании webpack, pug, babel и тд перед запуском сервера необходимо выполнять подготовительные команды.
Например, сборка кода в 1 файл, преобразование шаблонного кода в тот, что может разобрать браузер.
Для этого у npm есть команда `prestart`, автоматически выполняемая перед выполнением `start` скриптов (аналогично есть команда, выполняемая после старта)

    ```
    "scripts": {
        "prestart": "webpack && npm run compile-pug && npm run babel",
        "start": "node index.js",
        "test": "echo \"Error: no test specified\"",
        "compile-pug": "pug -c --extension 'pug.js' --name-after-file ./static",
        "babel": "babel static/dist/src -d static/dist --presets es2015",
        "eslint": "./node_modules/.bin/eslint './static/**/*.js'"
      }
    ```
2. Также необходимо следить, чтобы почти все зависимости были не в `devDependencies`, а в `dependencies`


### .gitignore

```text
# dependency directories
node_modules/

# IDE
.idea
.idea/

# Webpack result script
*app.js

# Webpack result script
*app.css

# Pug templates result
*.pug.js

# dist babel + Webpack result script & styles
/static/dist
```


### база проекта

* /static и /server

> Вся клиентская часть проекта будет в директории statiс, серверную можно оставить в корне (index.js)

* Импортирование классов

> Импортирова ***export default*** классы можно под любыми удобными именами ***import KEK from "./Router.js"***,
> Так как системе уже понятно, что импортировать, но можно и продублировать ***import Router from "./Router.js"***

* Взять элемент по классу

> document.querySelector(".element")

* static методы

> Не стоит сопротивляться подсказкам WebStorm :)
> Но там, где в вызове было this.staticMethod(), не забудте изменить на MyClass.staticMethod()


1. Создаем `MainApp.js`, точку входа в проект. Будет вызывать управляющий класс в данном примере `Router.js` (название кривое)

2. В `Router.js` навешиваются события на кнопки, отвечающие за роутинг ("игра", "логин", "назад" и тп) и отслеживаем историю в url

3. В `Router.js` вызываем класс `../views/main-page/MainPage.js`, отвечающий за работу `main-page` вьюшки.

4. Внутри класса `MainPage` необходимо реализовать рендеринг контента из шаблона `.pug`, а также логику на странице.

`constructor()` Рендерить части которые не изменны будем в конструкторе, затем навешиваем на отрисованные элементы события.


### BEM

`page__button_green`

вложенность мб сложнее
```
.main-box
    .page-1__form form // здесь форма и элемент блока-страницы, и самостоятельный блок
        .form__button-1 // внутри нее элементы кнопок
        .form__button-2
    .page-1__button
```


### babel

1. Устанавливаем `babel` по конспекту и проверяем зависимости в `package.json`

    ```json
    "dependencies": {
        ...
        "babel-cli": "^6.26.0",
        "babel-core": "^6.26.0",
        "babel-preset-es2015": "^6.24.1"
        }
    ```
2. Далее необходимо прописать команды в `package.json` для обработки сгенерированного `webpack`-кода к ES5 формату

    ```json
    "scripts": {
        "prestart": "webpack && npm run babel",
        ...
        "babel": "babel static/dist/src -d static/dist --presets es2015"
      }
    ```

    формат команды `babel SOURCE_WITH_ES6 -d DIST-READY`


### pug

1. Устанавливаем `pug`

    ```json
    "dependencies": {
        ...
        "pug": "^2.0.0-rc.4",
        "pug-cli": "^1.0.0-alpha6", 
        "pug-loader": "^2.3.0",     
        "style-loader": "^0.19.0"
        }
    ```

2. Добавляем команды в `package.json`

    ```json
    "scripts": {
        "prestart": "webpack && npm run compile-pug && npm run babel",
        ...
        "compile-pug": "pug -c --extension 'pug.js' --name-after-file ./static",
        }
    ```
    формат команды `pug -c --extension 'pug.js' --name-after-file ./WHERE_PUG_in_PROJ`
    
    Таким образом, все файлы `.pug` в папке с проектом (./static) будут скомпилированны в `.pug.js`.
    Такие файлы после сборки Webpack будут понятны браузеру.
    
3. Добавляем `loader` в  `webpack.config.js`

    ```js
    module: {
            rules: [
                {
                    test: /\.pug$/,
                    use: "pug-loader"
                }
            ]
        }
    ```
    
##### Пример

В управляющем классе страницы импортируем шаблон
```js
import template from "./main-page__pug.pug";
```
   
Теперь можно рендерить
```js
static renderPug() {
        // выбираем элемент для вставки контента
        let pugBox = document.querySelector(".main-page__pug");
        // указываем шаблон-источник(возможна передача параметров)
        pugBox.innerHTML = template();
        }
```
    
##### Пример рендера с параметрами(таблица лидеров)

Параметры в pug передаются через `locals` массив параметров
            
```js
    static renderLeaders(resp) {
            
            // устанавливаем локальные переменные из ответа для рендеринга
            // let locals = resp; 
            
            // устанавливаем локальные переменные из заглушки для рендеринга
            let locals = [
                {
                    login: "alex",
                    numberOfGames: 3,
                    record:444
                },
                {
                    login: "alex1",
                    numberOfGames: 3,
                    record:444
                },{
                    login: "alex2",
                    numberOfGames: 3,
                    record:444
                },{
                    login: "alex3",
                    numberOfGames: 3,
                    record:444
                },
            ];
    
            // рендерим шаблон
            leaderBoardBox.innerHTML = template(locals);
        }
``` 
    
В `pug` к параметрам можно также обращаться через `locals`. В данном примере итерируемся по массиву парпаметров. 
```jade
table.records-page__table(align="center")
        tr
            th User
            th Number
            th Score
        each user in locals
            tr
                td= user.login
                td= user.numberOfGames
                td= user.record
```


### смена темы

Динамически наполняем или очищаем тэг `<style class="theme-styles"></style>` в index.html


# проект без интернета

### service worker

1. В директории со статикой создаем файл,
который будет отвечать за регистрацию ServiceWorker
`./static/servicw-worker.js`

2. Подключаем его к index.html 
```html
    <script src="service-worker.js"></script>
    <script src="dist/app.js"></script>
```

3. Добавляем регистрацию ServiceWorker  в точку входа в приложение. В данном случае в `MainApp.js`.
Чтобы запустить ServiceWorker, его для начала необходимо зарегистрировать.
В наше приложение нужно добавить код следующего содержания

```js
    // здесь будет проверка service-workerа
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('service-worker.js')
            .then(function (registration) {
                // при удачной регистрации имеем объект типа ServiceWorkerRegistration
                console.log('ServiceWorker registration', registration);
            })
            .catch(function (err) {
                // throw new Error('ServiceWorker error: ' + err);
                console.error('Registration err', err);
            });
    }
```
4. Следующим шагом пишем простой код ServiceWorker’а.

> Важно иметь в виду, что serviceWorker никак не связан с глобальной областью видимости приложения,
в котором был зарегистрирован, и вообще не имеет объекта window.
В `self` у него находится объект типа ServiceWorkerGlobalScope,
на котором устанавливаются обработчики событий.

> Далее в коде serviceWorker’а на этапе инсталляции
создаём первоначальный кэш из необходимых ресурсов.
Обратите внимание, все ссылки указаны относительно корня.
>> Также среди кэшируемых ресурсов нет файла workerLoader.js,
который регистрирует serviceWorker. Его кэшировать не желательно,
т.к. в режиме offline приложение и без него будет работать.
Но если срочно будет необходимо отключить serviceWorker,
могут возникнуть проблемы. Пользователи вынуждены будут ждать
пока serviceWorker обновится самостоятельно
(посредством сравнения содержимого).

```js
// наименование для нашего хранилища кэша
const CACHE_NAME = 'front-sample_serviceworker_v2';
// ссылки на кэшируемые ФАЙЛЫ И ПУТИ РОУТИНГА относительно текущей директории
const cacheUrls = [
    "/",

    "dist/app.js",

    "styles/main.css",
    "views/main-page/main-page.css",
];

this.addEventListener("install", function (event) {
    event.waitUntil(
        // находим в глобальном хранилище Cache-объект с нашим именем
        // если такого не существует, то он будет создан
        caches.open(CACHE_NAME)
            .then(function (cache) {
                // загружаем в наш cache необходимые файлы
                return cache.addAll(cacheUrls);
            })
            .then(
                // сразу активируем текущую версию
                () => {this.skipWaiting();}
            )
    );
});

this.addEventListener("fetch", function (event) {
    event.respondWith(
        // ищем запрашиваемый ресурс в хранилище кэша
        caches.match(event.request).then(function (cachedResponse) {
            // выдаём кэш, если он есть
            if (cachedResponse) {
                return cachedResponse;
            }
            // иначе запрашиваем из сети как обычно
            return fetch(event.request);
        })
    );
});

// Promise переданный в waitUntil() заблокирует другие события до своего завершения,
// поэтому можно быть уверенным, что процесс очистки закончится раньше,
// чем выполнится первое событие fetch на новом кеше.
this.addEventListener("activate", function(event) {
    let cacheWhitelist = [CACHE_NAME];
    event.waitUntil(
        caches.keys()
            .then(function(keyList) {
                return Promise.all(keyList.map(function(key) {
                    if (cacheWhitelist.indexOf(key) === -1) {
                        return caches.delete(key);
                    }
                }));
            })
    );
});

```
    
### manifest.json  
   Подключаем в index.html
   `<link rel="manifest" href="manifest.json">`

# структура проекта, за которую не били
├── static

│   ├── dist `# директория с файлами, готовыми для отдачи`

│   │   ├── app.css

│   │   ├── app.js

│   │   └── src

│   │   ───└─ app.js

│   ├── favicon.ico

│   ├── game-modules `# директория с игровой логикой`

│   │   ├── GameManager.js

│   │   ├── libs

│   │   ├── ObjectsCreater.js

│   │   └── SceneRenderer.js

│   ├── img

│   │   ├── icon

│   │   ├── logo.png

│   │   ├── multi.png

│   │   └── single.png

│   ├── index.html

│   ├── manifest.json `# конфиг для скачивания офлайн версии`

│   ├── modules `# директория с основной логикой приложения`

│   │   ├── Debugger.js

│   │   ├── fieldsCleaner.js

│   │   ├── FormValidator.js

│   │   ├── LoginForm.js

│   │   ├── MainApp.js

│   │   ├── MessageBox.js

│   │   ├── Page.js

│   │   ├── PagePresenter.js

│   │   ├── RegisterForm.js

│   │   ├── RequestToHost.js

│   │   ├── Router.js

│   │   ├── ThemeChanger.js

│   │   ├── themeStyles.js

│   │   ├── TouchDelegater.js

│   │   └── whoamiMixin.js

│   ├── service-worker.js `# для кэширования файлов, необходимых в офлайне`

│   ├── styles `# директория со стилями, общими для всего приложения`

│   │   ├── form.scss

│   │   ├── message-box.scss

│   │   └── style.scss

│   └── views `# в поддиректориях ресурсы для каждой из страниц`

│       │        `(управляющий модуль(.js) + шаблоны(.pug) + стили(.scss))`

│       │        `файлы (.pug.js) появляются только после `pug`-компиляции`

│       ├── info-page

│       │   ├── InfoPage.js

│       │   ├── info-page.pug

│       │   ├── info-page.pug.js

│       │   └── info.scss

│       ├── login-page

│       │   ├──   ...

│       ├── main-page

│       │   ├── MainPage.js

│       │   ├── main-page.pug

│       │   ├── main-page.pug.js

│       │   ├── main.scss

│       │   ├── playmenu.pug

│       │   ├── playmenu.pug.js

│       │   └── playmenu.scss

│       ├── multiplay-page

│       │   ├──   ...

│       ├── play-page

│       │   ├──   ...

│       ├── records-page

│       │   ├── leaderboard.pug

│       │   ├── leaderboard.pug.js

│       │   ├── RecordsPage.js

│       │   ├── records-page.pug

│       │   ├── records-page.pug.js

│       │   └── records.scss

│       └── register-page

│       │   ├──   ...
├── package.json

├── package-lock.json

├── README.md

└── webpack.config.js

### лол
### кек
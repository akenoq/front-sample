"use strict";

import template from "./main-page__pug.pug";

// все страницы следует наследоваь от общего класса Page, так как будут однотипные методы
class MainPage {
    constructor() {
        MainPage.renderPug();
        console.log("1");
        MainPage.addEventOnButton();
        console.log("2");
    }

    static renderPug() {
        let pugBox = document.querySelector(".main-page__pug");
        pugBox.innerHTML = template();
    }

    // этот метод лучше пустым определить в классе Page, а тут переопределить
    static addEventOnButton() {
        document.querySelector(".main-page__button").addEventListener("click", () => alert("HELLO!"));
    }
}

export default MainPage

"use strict";

import Router from "./Router.js";

class MainApp {
    constructor () {
        // создать для отладочного вывода класс дебаггер
        console.log("Application was created");

        // здесь будет проверка service-workerа
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('service-worker.js')
                .then(function (registration) {
                    // при удачной регистрации имеем объект типа ServiceWorkerRegistration
                    console.log('ServiceWorker registration', registration);
                })
                .catch(function (err) {
                    // throw new Error('ServiceWorker error: ' + err);
                    console.error('Registration err', err);
                });
        }

        const router = new Router();
    }
}

window.addEventListener("load", function () {
    new MainApp();
    document.querySelector(".main-box").hidden = false;
});

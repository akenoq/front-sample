"use strict";

import MainPage from "../views/main-page/MainPage.js";

class Router {
    constructor() {
        new MainPage();
    }
}

export default Router